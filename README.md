**Checking / Installing**
to check if the nodejs already installed:
node --version

download: https://nodejs.org/en/ 

check if mongoDB already installed:
mongo --version

to install mongo: https://docs.mongodb.com/manual/installation/

**Checking / Initializing the Dummy Data JSON**
Please check the **dummy_data.json** before running the app.
Kindly, change the "**email**" to your **personal or test email** you want to use during development.

**Running the sample app**
After cloning, 
- start mongodb by running to your terminal: "**mongod**"
- go to the directory of app.js, then run: "**node app.js**" 
- **check** the **mongodb** first to be sure that the data/db is properly created: 
    Open another window for terminal > run **mongo** > **show dbs** > **use chatDB** > **show collections** > **db.chatData.find()**
    it should return the data from mongo.
- access the app in browser: **localhost:3000/chatbot**


**NOTE:**
**if there's any changes to your or everytime you need to test your code, please terminate then run it again..**
** control + c to the terminal > run node app.js**

**Testing Chatbot / DialogFlow**
to inquire for your account type:

	i want to know about my account
	i want to check my account

to request for account information:

	i want to request an account information
	i want to request account information
	i want to request my account information

to ask about your bill, or request billing statement:

	i want to know about my bill
	i want to request billing statement
	request billing statement


to update account name:

	i want update my account
	i want to change my account name
	change my account name


to topup to your account:

	i want to add credits to my account
	add credits to my account


samples:

I want to update my account > change my account name > *instruction from bot*
I want to know about my account > I want to check my account information > *instruction from bot*
