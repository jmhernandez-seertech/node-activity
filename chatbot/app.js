var express = require('express');
var nunjucks = require('nunjucks');
var chat_bot = require('./middleware/chat_bot');
var MongoClient = require('mongodb').MongoClient;
var dummy_data = require('./dummy_data')


var app = express();

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

var url = 'mongodb://localhost:27017/chatDB';

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  console.log("connected to mongo!");
  db.listCollections().toArray(function(err, result){
  	if(result.length <= 0){
  		dummy_data.forEach(function(data, err){
  			db.collection('chatData').insertOne(data, function(err,res){
  				console.log("inserted!")
  				db.close()
  			});
  		});
  	}
  	db.close()
  });
});

app.set('port', process.env.PORT || 3000)
app.use('/node_modules', express.static('node_modules'));
app.use('/static', express.static('static'));

chat_bot(app);

var server = app.listen(app.get('port'),function(){
	console.log("stating node app..")
});
