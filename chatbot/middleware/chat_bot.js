var bodyParser = require('body-parser')
var apiai = require('apiai');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var assert = require('assert');
var nodemailer = require('nodemailer');
var appAI = apiai("b13bd4ff2e3042ce9e7870162329e60f");

var urlencodedParser = bodyParser.urlencoded({extended: false});

var url = 'mongodb://localhost:27017/chatDB';



var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'nodejs.testbot@gmail.com',
    pass: 'q1w2e3r4t5y6u7i8o9p0'
  }
});


var dummy_account = []

function connectMongo(){
	MongoClient.connect(url, function(err, db) {
	  assert.equal(null, err);
	  console.log("Connected correctly to server.");
	  var getData = db.collection('chatData').find()
	  getData.forEach(function(doc, err){
	  	assert.equal(null, err);
	  	dummy_account.push(doc)
	  })
	  db.close();
	});
};
connectMongo();//connects to the DB and gets the existing data and stores it in array in dummy_account

var custID = ""

var mailOptions = {
			  from: 'nodejs.testbot@gmail.com',
			  to: '',
			  subject: '',
			  html: ''
			};


//maps the option that the bot can do.
var cust_request_map = [
	{'id':1, 'option':'account topup'},
	{'id':2, 'option':'account update'},
	{'id':3, 'option':'account information'},
	{'id':4, 'option':'bill statement'},
]


function updateAccount(data,cust_req){
	MongoClient.connect(url, function(err, db) {
	  assert.equal(null, err);

	  db.collection('chatData').update({ _id: data._id }, { $set: data.payload })
	  db.close();
	});
}

function botEmail (data,cust_req){
	MongoClient.connect(url, function(err, db) {
	  assert.equal(null, err);
	  var getData = db.collection('chatData').find(data._id)
	  getData.forEach(function(doc, err){
	  	if(cust_req.id == 3 || cust_req.id == 2 || cust_req.id == 1){
	  		mailOptions.to = doc.email
			mailOptions.subject = data.email_subject
			mailOptions.html = '<p>Account Name: '+doc.fname+' '+doc.lname+'</p>' +
							   '<p>Age: '+doc.age+'</p>'+
							   '<p>Username: '+doc.username+'</p>' +
							   '<p>Amount Credits: '+doc.amt_credits+'</p>'
		}else if(cust_req.id == 4){
			mailOptions.to = doc.email
			mailOptions.subject = data.email_subject
			mailOptions.html = '<p>Account Name: '+doc.fname+' '+doc.lname+'</p>' +
							   '<p>Age: '+doc.age+'</p>'+
							   '<p>Username: '+doc.username+'</p>'+
							   '<p>Amount Credits: '+doc.amt_credits+'</p>'+
							   '<h3> Bill Statement </h3>' +
							   '<p> Total Amount Due:'+doc.bill_statement.total_amount_due+'</p>'+
							   '<p> Due Date:'+doc.bill_statement.due_date+'</p>'+
							   '<p> Latest Payment Date:'+doc.bill_statement.latest_payment_date+'</p>'+
							   '<p> Original Amount Loan:'+doc.bill_statement.orig_amt_loan+'</p>'+
							   '<p> Total Amount Paid:'+doc.bill_statement.total_paid+'</p>'+
							   '<p> Monthly Due:'+doc.bill_statement.monthly+'</p>'+
							   '<p> Remaining Balance:'+doc.bill_statement.remaining_bal+'</p>'+
							   '<p> Amount Past Due:'+doc.bill_statement.amt_past_due+'</p>'
		}
	  	
		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				console.log(error);
			}else {
				console.log('Email sent: ' + info.response);
			}
		});
	  })
	  db.close();
	});
}



module.exports = function(app) {

app.get('/chatbot', function(req, res){
	res.render('index.html', {})
});

//initialize the greetings of the bot
app.get('/chatbot/init-msg', function(req, res){
	var request = appAI.textRequest('get_name', {
	    sessionId: '<unique session id>'
	});
	 
	request.on('response', function(response) {
	    console.log(response.result.fulfillment.speech)
	    var ai_res = response.result.fulfillment.speech;
	    var bot_res = {
	    	'txtMsg': ai_res,
	    	'sender': 'bot'
	    }
	    res.json(bot_res)
	});
	 
	request.on('error', function(error) {
	    console.log(error);
	});
	request.end();
});

//this object manipulates the response that will send to index.js
var bot_res = {
	'txtMsg': '',
	'sender': 'bot',
	'data_update': false,
	'cust_id': false,
	'cust_request': 0
}

app.post('/chatbot', urlencodedParser, function(req, res){
	var data = req.body //this the request from the user contains object
	var inputText = data.txtMsg //the actual message sent by the user
	//receives the user chat/request and send it to DialogFlow/api.ai
	var request = appAI.textRequest(inputText, {
	    sessionId: '<unique session id>'
	});

	request.on('response', function(response) {//response is the result from DialogFlow
		var ai_params = response.result.parameters //gets the parameters from results
		if(ai_params['inquiry'] == 'account' && ai_params['account'] == 'information'){
			//the value for this variable is the text response of the bot from DialogFlow
		 	var ai_res = response.result.fulfillment.speech;
		 	console.log(ai_params)//this prints the parameters as object
		 	bot_res.txtMsg = ai_res
		 	bot_res.cust_id = true
		 	bot_res.cust_request = 3
		}else if(ai_params['bill'] == 'statement' && ai_params['inquiry'] == 'bill' ){
			//the value for this variable is the text response of the bot from DialogFlow
		 	var ai_res = response.result.fulfillment.speech;
		 	console.log(ai_params)
		 	bot_res.txtMsg = ai_res
		 	bot_res.cust_id = true
		 	bot_res.cust_request = 4
		}else if(ai_params['account'] == 'update' && ai_params['change_account'] == 'name' ){
			//the value for this variable is the text response of the bot from DialogFlow
			var ai_res = response.result.fulfillment.speech; 
		    console.log("bot_res", bot_res)
		    bot_res.txtMsg = ai_res
		    bot_res.cust_request = 2
		    bot_res.data_update = true
		}else if(ai_params['account'] == 'topup' && ai_params['inquiry'] == 'account' ){
			//the value for this variable is the text response of the bot from DialogFlow
			var ai_res = response.result.fulfillment.speech; 
		    console.log("bot_res", bot_res)
		    bot_res.txtMsg = ai_res
		    bot_res.cust_request = 1
		    bot_res.data_update = true
		}else{
			//the value for this variable is the text response of the bot from DialogFlow
			var ai_res = response.result.fulfillment.speech; 
		    console.log(ai_params)
		    bot_res.txtMsg = ai_res
		    bot_res.cust_request = ''
		}
	    
	    res.json(bot_res)//send the bot_res object to index.js
	});
	 
	request.on('error', function(error) {
	    console.log(error);
	});
	request.end();
});


app.post('/chatbot/update/', urlencodedParser, function(req, res){
	var data = req.body //this the request from the user contains object
	var input = data.txtMsg.split(",")
	var custID = input[0].trim()

	//console.log(custID)
	var cust_req = cust_request_map.find(function(item){return item.id == data.cust_request})

	var filter_data = dummy_account.filter(function(res){
			return res.cust_id == custID 
		})
	
	if(filter_data.length == 0){
			bot_res.txtMsg = "Sorry, your ID was not found in our system."
	}else{
		//this will set the email subject and bot response
		var payload = {}
		if(cust_req.id == 1){
			var amt_credits = parseInt(input[1].trim())
			var cur_credits = filter_data[0]['amt_credits']

			payload['amt_credits'] = amt_credits + cur_credits
			filter_data[0]['payload'] = payload;

			updateAccount(filter_data[0],cust_req)
		    bot_res.txtMsg = "Alright. " + amt_credits + " credits will be added to your account."
		}else if(cust_req.id == 2){
			var fname = input[1].trim()
			var lname = input[2].trim()

			// code for edit account information
			if (fname != '' | fname != null) {
				payload['fname'] = fname
			} else {
				fname = ''
			}

			if (lname != '' | lname != null) {
				payload['lname'] = lname
			} else {
				lname = ''
			}

			filter_data[0]['payload'] = payload;
			updateAccount(filter_data[0],cust_req)
		    bot_res.txtMsg = "Alright. Your name will be changed to: " + fname + " " + lname
		}
	}
	
	
    bot_res.data_update = false
   	bot_res.cust_id =  false
   	bot_res.cust_request = 0

	res.json(bot_res)
})

app.post('/chatbot/request/information', urlencodedParser, function(req, res){
	var data = req.body //this the request from the user contains object
	var custID = data.txtMsg //
	var cust_req = cust_request_map.find(function(item){return item.id == data.cust_request})

	var filter_data = dummy_account.filter(function(res){
			return res.cust_id == custID 
		})
	//console.log(filter_data)
	if(filter_data.length == 0){
			bot_res.txtMsg = "Sorry, your ID was not found in our system."
	}else{
		//this will set the email subject and bot response
		if(cust_req.id == 3){
			filter_data[0]['email_subject'] = "Requested Account Information"
			botEmail(filter_data[0],cust_req)
		    bot_res.txtMsg = "Alright. Your account information will be send to your email : " + filter_data[0].email
		}else if(cust_req.id == 4){
			filter_data[0]['email_subject'] = "Requested Bill Statement"
			botEmail(filter_data[0], cust_req)
		    bot_res.txtMsg = "Alright. Your Bill Statement will be send to your email : " + filter_data[0].email
		}
	}
	
	
    bot_res.data_update = false
   	bot_res.cust_id =  false
   	bot_res.cust_request = 0

	res.json(bot_res)
})


};








