

var TestPage = React.createClass({
	getInitialState: function(){
      return {
        prog_val:[],
        data_update: false,
        cust_id: false,
        cust_request: 0
      };
    },

    chatBotCall:function(url,data,req){
    	$.ajax({
	          url: url,
	          type: req,
	          data: data,
	          success: function(res){
	          	var currMSg = this.state.prog_val
	  			currMSg.push(res)
	          	this.setState({
	          		prog_val:currMSg,
	          		data_update:res.data_update,
	          		cust_id:res.cust_id,
	          		cust_request:res.cust_request
	          	})
	          	$("#chatBox").scrollTop($("#chatBox").height()*50);
	          }.bind(this),
	          error: function(res) {
	          	console.log('error', res)
	          }.bind(this)
	    })
    },
    sendData:function(){
    	var txtMsg = $("#txtMsg").val()
    	if(txtMsg == '' || txtMsg == null){
    		$("#txtMsgValidator").addClass('has-error has-feedback')
    	}else{
    		var data = { 
    				txtMsg: txtMsg,
    				sender: 'user',
    				cust_request: this.state.cust_request
    			}
    		var arr_msg = this.state.prog_val
    		arr_msg.push(data)
    		console.log(arr_msg)
    		this.setState({prog_val:arr_msg})
    		if(this.state.data_update){
    			this.chatBotCall('/chatbot/update',data,'POST')
    			this.setState({data_update:false})
    		}else if(this.state.cust_id){
    			console.log("this is data -- ", data)
    			this.chatBotCall('/chatbot/request/information',data,'POST')
    		}else{
    			console.log("this is data -- ", data)
    			this.chatBotCall('/chatbot',data,'POST')
    		}
    		$("#txtMsgValidator").removeClass('has-error has-feedback')
    	}
    	$("#txtMsg").val('')
    	
    },
    handleSend: function(){
    	this.sendData();
    },
    handleKeyPress: function(event){
    	if(event.key == 'Enter'){
  			this.sendData();
    	}
    },
    componentWillMount: function(){
    	$.ajax({
		          url: '/chatbot/init-msg',
		          type: 'GET',
		          success: function(res){
		          	var init_msg = []
		          	init_msg.push(res)
		          	this.setState({prog_val:init_msg})
		          }.bind(this),
		          error: function(res) {
		          	console.log('error', res)
		          }.bind(this)
		      })
    },
	render: function(){
		return(
			<div>
				<div className="container">
					<div className="col-md-12">
						<div className="row">
							<div id="todoBorder" className="col-md-7 col-md-offset-2 todoBorder">
								<div className="col-md-12" style={{"padding":"0px"}}>
									<div id="todoNav" className="todoBar">
										<h4 style={{"color":"white","padding":"10px","margin":"0px"}}>Basic Chatbot</h4>
									</div>
								</div>
								<div id="chatBox" className="col-md-12" style={{"overflowY":"auto","height":"370px"}}>
									{this.state.prog_val.length > 0 ? this.state.prog_val.map((value,i)=>{
							  				return(
						                        <div className="col-sm-12" key={i} style={{"padding":"6px"}}>
						                          <div className="col-sm-10">
						                            <strong>{value.sender + " : "}</strong>{value.txtMsg} 
						                          </div>
						                        </div>
						                      )
							  			}):(null)
							  		} 
								</div>
							</div>
							<div id="todoBorder" className="col-md-7 col-md-offset-2" style={{"padding":"0px"}}>
								<div className="row">
									<div id="txtMsgValidator" className="col-sm-11">
										<div className="input-group">
											<span className="input-group-addon">Message:</span>
											<input id="txtMsg" type="text" className="form-control" aria-label="send message" onKeyPress={this.handleKeyPress}/>
										</div>
									</div>
									<div id="todoBorder" className="col-sm-1" style={{"margin":"0px","padding":"0px"}}>
										<button id="btnAdd" className="btn btn-primary" style={{"fontSize":"20px"}} onClick={this.handleSend}>
										<span className="glyphicon glyphicon-send"></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
});

ReactDOM.render(
			<TestPage/>,
			document.getElementById('testMount')
		);





